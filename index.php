<?php
	include_once "vendor/autoload.php";

	use MongoDB\Client;

	$m = new Client();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json");

	$db = $m->selectDatabase("tms_4");
	
	if ( isset($_GET["collection"]) ) {
		if ( $_GET["collection"] == "app" ) {
			$app = $db->selectCollection("app");
			$appData = $app->find();
			$appSeries = [];
			$i = 0;
			foreach ( $appData as $appRecord ) {
				$appSeries[] = [ $i, $appRecord['TotalMilliseconds'] ];
				$i++;
			}

			$series = [
				[
					"name" => "App",
					"data" => $appSeries
				]
			];
		}
		else if ( $_GET["collection"] == "perf" ) {
			$perf = $db->selectCollection("perf");
			
			$perfData = collect( $perf->find() );
			$grouped = $perfData->groupBy("ServiceName");

			$series = [];

			foreach ( $grouped as $key => $value ) {
				$i = 0;
				$series[] = [
					"name" => $key,
					"data" => $value->map( function( $item ) use (&$i) {
						$res = [$i, $item["TotalMilliseconds"]];
						$i++;
						return $res;
					})
				];
			}
		}

		print json_encode($series);
	}