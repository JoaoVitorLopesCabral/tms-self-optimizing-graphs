**Run 'composer install' before using**

Required extensions: php-mongodb

Recommend installing php and extensions from Ondrej's PPA
[PPA](https://launchpad.net/~ondrej/+archive/ubuntu/php)

After installing the PPA, run the following commands:

```bash
sudo apt-get update

sudo apt-get install php-mongodb
```